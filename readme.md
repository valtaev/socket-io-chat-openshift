# socket-io-chat-openshift

This OpenShift hosing ready version of Chat Example code. You can find Original source [here](https://github.com/rauchg/chat-example).

For additional information see [Getting Started](http://socket.io/get-started/chat/) guide 
of the Socket.IO website.